// RiveScript-JS
//
// Node.JS Web Server for RiveScript
//
// Run this and then communicate with the bot using `curl` or your favorite
// REST client. Example:
//
// curl -i \
//    -H "Content-Type: application/json" \
//    -X POST -d '{"username":"soandso","message":"hello bot"}' \
//    http://localhost:2001/reply


function success22_handler() {
     var pgp = require('pg-promise')(/*options*/);


// alternative:
 var cn = "postgres://root:123456@localhost:5432/aiteste";

var db = pgp(cn); // database instance;

  var x = db.one('INSERT INTO public.question(id, query, aceite) VALUES($1, $2, $3) RETURNING id', ['10','ola10', true]).then(data => {

pgp.end(); return "x";
    })
    .catch(error => {
pgp.end();
    return "x2";
    })
}



var express = require("express"),
    bodyParser = require("body-parser"),
    RiveScript = require("./lib/rivescript.js");

var stamentsaveSignal;
var stamentQustion;
var stamentAnswer;
var cors = require('cors');
// Create the bot.
var bot = new RiveScript({utf8: true});
success_handler(0);
//bot.loadDirectory("./brain", success_handler, error_handler);

function success_handler(loadcount) {
    console.log("Load #" + loadcount + " completed!");
    bot.stream("! version = 2.0\n! sub joaquim = aiden\n! var name = Aiden\n+ (what is my name|who am i|do you know my name|do you know who i am)\n- Your name is <get name>.\n+ my name is <bot name>\n- <set name=<bot name>>What a coincidence! That's my name too!\n+ola aiden\n-ola");
    bot.sortReplies();
    var deparsed = bot.deparse();
    bot.write("custom2.rive",deparsed);
    // Set up the Express app.
    var app = express();
    app.use(cors());

    stamentsaveSignal=false;
    stamentAnswer = [];
    stamentQustion="";
    // Parse application/json inputs.
    app.use(bodyParser.json());
    app.set("json spaces", 4);

    // Set up routes.
    app.post("/reply", getReply);
    app.get("/", showUsage);
    app.get("*", showUsage);

    // Start listening.
    app.listen(2001, function() {
        console.log("Listening on http://localhost:2001");
    });
}

function success_handler2(loadcount) {
    console.log("Load #" + loadcount + " completed!");

    bot.sortReplies();
}

function error_handler(loadcount, err) {
    console.log("Error loading batch #" + loadcount + ": " + err + "\n");
}

// POST to /reply to get a RiveScript reply.
function getReply(req, res) {
    // Get data from the JSON post.
    var username = req.body.username;
    var message = req.body.message;
    var vars = req.body.vars;

    // Make sure username and message are included.
    if (typeof(username) === "undefined" || typeof(message) === "undefined") {
        return error(res, "username and message are required keys");
    }

    // Copy any user vars from the post into RiveScript.
    if (typeof(vars) !== "undefined") {
        for (var key in vars) {
            if (vars.hasOwnProperty(key)) {
                bot.setUservar(username, key, vars[key]);
            }
        }
    }

    var reply = bot.reply(username, message, this);

    if (reply.indexOf("<stamentsave>")!=-1) {
        reply  = reply.replace("<stamentsave>","");
        stamentsaveSignal=true;
    stamentAnswer = [];
    stamentQustion="";

    }
    if(stamentsaveSignal)
    {
        if(reply.indexOf("<question>")!=-1)
        {
            stamentQustion= "+"+reply.substring(reply.indexOf("<question>")+"<question>".length,reply.indexOf("</question>"))+"\n";
            reply=reply.substring(reply.indexOf("</question>")+"</question>".length, reply.length);
        }
        if(reply.indexOf("<answer>")!=-1)
        {
            stamentAnswer.push("-"+reply.substring(reply.indexOf("<answer>")+"<answer>".length,reply.indexOf("</answer>"))+"\n");
            reply=reply.substring(reply.indexOf("</answer>")+"</answer>".length, reply.length);
        }
    }

    if (stamentsaveSignal&&reply.indexOf("</stamentsave>")!=-1) {
        reply  = reply.replace("</stamentsave>","");
        stamentsaveSignal=false;
         console.log(stamentQustion+stamentAnswer.join(""));
        bot.stream(stamentQustion+stamentAnswer.join(""));
    bot.sortReplies();
    }
    // Get all the user's vars back out of the bot to include in the response.
    vars = bot.getUservars(username);

    // Send the JSON response.
    res.json({
        "status": "ok",
        "reply": reply,
        "vars": vars
    });
}

// All other routes shows the usage to test the /reply route.
function showUsage(req, res) {
    var egPayload = {
        "username": "soandso",
        "message": "Hello bot",
        "vars": {
            "name": "Soandso"
        }
    };
    res.writeHead(200, {
        "Content-Type": "text/plain"
    });
    res.write("Usage: curl -i \\\n");
    res.write("   -H \"Content-Type: application/json\" \\\n");
    res.write("   -X POST -d '" + JSON.stringify(egPayload) + "' \\\n");
    res.write("   http://localhost:2001/reply");
    res.end();
}

// Send a JSON error to the browser.
function error(res, message) {
    res.json({
        "status": "error",
        "message": message
    });
}
