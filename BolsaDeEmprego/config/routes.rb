BolsaDeEmprego::Application.routes.draw do

  get "newsletter/new"
  get "password_resets/new"
  get "password_resets/edit"
  resources :oferta

  resources :candidatos

  resources :entidades
  #  resources :interesses

  resources :noticia, path: 'backoffice/noticia'
  resources :sessions, only: [:new, :create, :destroy]
  resources :users, path: 'backoffice/users'
  resources :password_resets,     only: [:new, :create, :edit, :update]


  match 'backoffice/:id/editpassword',  to: 'users#editpassword',       via: 'get',     as: 'editpassword_user'
  match '/:id/editpassword',            to: 'users#editpasswordFO',     via: 'get',     as: 'editpassword_frontoffice_user'
  match '/:id/updatePassword',          to: 'users#updatePassword',     via: 'put'
  match 'noticia',                      to: 'noticia#indexFrontOffice', via: 'get',     as: 'noticia_frontoffice'
  match 'noticia/:id',                  to: 'noticia#showFrontOffice',  via: 'get',     as: 'noticium_frontoffice'
  match '/',                            to: 'home#index',               via: 'get',     as: 'home'
  match 'backoffice/',                  to: 'home#indexBackOffice',     via: 'get',     as: 'home_backoffice'
  match 'candidatos/profile/:id',       to: 'candidatos#profile',       via: 'get',     as: 'candidato_profile'
  match 'entidades/profile/:id',        to: 'entidades#profile',        via: 'get',     as: 'entidade_profile'
  match 'interesseD/:id',               to: 'interesse#destroy',        via: 'delete',  as: 'del_interesse'
  match 'interesseC/:id',               to: 'interesse#create',         via: 'post',    as: 'cre_interesse'
  match 'candidaturaC/:id',             to: 'candidatura#create',       via: 'post',    as: 'cre_candidatura'
  match 'candidaturaD/:id',             to: 'candidatura#destroy',      via: 'delete',  as: 'del_candidatura'
  match 'oferta/activar_oferta/:id',    to: 'oferta#activar',           via: 'post',    as: 'activar_oferta'
  match 'oferta/desactivar_oferta/:id', to: 'oferta#desactivar',        via: 'delete',  as: 'desactivar_oferta'
  match '/signin',                      to: 'sessions#new',             via: 'get'
  match '/signup',                      to: 'users#regist',             via: 'get'
  match '/signout',                     to: 'sessions#destroy',         via: 'delete'
  root  'sessions#home'
end



#namespace :backoffice do
#  resources :noticia,
#  :users do
#        member do
#          get :editpassword
#        end
#      end
#end
