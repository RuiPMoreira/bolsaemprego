# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170328082004) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "area_profissionals", force: true do |t|
    t.string   "designacao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "atividade_profissionals", force: true do |t|
    t.string   "designacao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "candidato_entidades", force: true do |t|
    t.integer  "candidato_id"
    t.integer  "entidade_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "candidato_entidades", ["candidato_id", "created_at"], name: "index_candidato_entidades_on_candidato_id_and_created_at", using: :btree
  add_index "candidato_entidades", ["candidato_id", "entidade_id"], name: "index_candidato_entidades_on_candidato_id_and_entidade_id", unique: true, using: :btree
  add_index "candidato_entidades", ["entidade_id", "created_at"], name: "index_candidato_entidades_on_entidade_id_and_created_at", using: :btree

  create_table "candidatos", force: true do |t|
    t.integer  "user_id"
    t.string   "morada"
    t.string   "codigoPostal"
    t.integer  "contacto"
    t.integer  "contactoSec"
    t.string   "pagina"
    t.date     "data"
    t.integer  "bi"
    t.integer  "area_prof_id"
    t.integer  "habilitacao_id"
    t.integer  "situacao_prof_id"
    t.integer  "concelho_id"
    t.text     "apresentacao"
    t.text     "habilitacoes"
    t.text     "exp_profissional"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "foto_file_name"
    t.string   "foto_content_type"
    t.integer  "foto_file_size"
    t.datetime "foto_updated_at"
    t.string   "cv_file_name"
    t.string   "cv_content_type"
    t.integer  "cv_file_size"
    t.datetime "cv_updated_at"
  end

  add_index "candidatos", ["area_prof_id", "created_at"], name: "index_candidatos_on_area_prof_id_and_created_at", using: :btree
  add_index "candidatos", ["concelho_id", "created_at"], name: "index_candidatos_on_concelho_id_and_created_at", using: :btree
  add_index "candidatos", ["habilitacao_id", "created_at"], name: "index_candidatos_on_habilitacao_id_and_created_at", using: :btree
  add_index "candidatos", ["situacao_prof_id", "created_at"], name: "index_candidatos_on_situacao_prof_id_and_created_at", using: :btree
  add_index "candidatos", ["user_id", "created_at"], name: "index_candidatos_on_user_id_and_created_at", using: :btree

  create_table "candidaturas", force: true do |t|
    t.integer  "candidato_id"
    t.integer  "oferta_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "candidaturas", ["candidato_id", "oferta_id"], name: "index_candidaturas_on_candidato_id_and_oferta_id", unique: true, using: :btree
  add_index "candidaturas", ["candidato_id"], name: "index_candidaturas_on_candidato_id", using: :btree
  add_index "candidaturas", ["oferta_id"], name: "index_candidaturas_on_oferta_id", using: :btree

  create_table "concelhos", force: true do |t|
    t.string   "designacao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "entidades", force: true do |t|
    t.integer  "user_id"
    t.string   "morada"
    t.string   "codigoPostal"
    t.integer  "contacto"
    t.integer  "contactoSec"
    t.string   "pagina"
    t.integer  "nif"
    t.integer  "atividade_prof_id"
    t.text     "apresentacao"
    t.integer  "concelho_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "foto_file_name"
    t.string   "foto_content_type"
    t.integer  "foto_file_size"
    t.datetime "foto_updated_at"
  end

  add_index "entidades", ["atividade_prof_id", "created_at"], name: "index_entidades_on_atividade_prof_id_and_created_at", using: :btree
  add_index "entidades", ["concelho_id", "created_at"], name: "index_entidades_on_concelho_id_and_created_at", using: :btree
  add_index "entidades", ["user_id", "created_at"], name: "index_entidades_on_user_id_and_created_at", using: :btree

  create_table "interesse_ces", force: true do |t|
    t.integer  "candidato_id"
    t.integer  "entidade_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "interesse_ces", ["candidato_id", "created_at"], name: "index_interesse_ces_on_candidato_id_and_created_at", using: :btree
  add_index "interesse_ces", ["candidato_id", "entidade_id"], name: "index_interesse_ces_on_candidato_id_and_entidade_id", unique: true, using: :btree
  add_index "interesse_ces", ["entidade_id", "created_at"], name: "index_interesse_ces_on_entidade_id_and_created_at", using: :btree

  create_table "interesse_ecs", force: true do |t|
    t.integer  "entidade_id"
    t.integer  "candidato_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "interesse_ecs", ["candidato_id", "created_at"], name: "index_interesse_ecs_on_candidato_id_and_created_at", using: :btree
  add_index "interesse_ecs", ["entidade_id", "candidato_id"], name: "index_interesse_ecs_on_entidade_id_and_candidato_id", unique: true, using: :btree
  add_index "interesse_ecs", ["entidade_id", "created_at"], name: "index_interesse_ecs_on_entidade_id_and_created_at", using: :btree

  create_table "interesses", force: true do |t|
    t.integer  "origem_id"
    t.integer  "destino_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "interesses", ["destino_id"], name: "index_interesses_on_destino_id", using: :btree
  add_index "interesses", ["origem_id", "destino_id"], name: "index_interesses_on_origem_id_and_destino_id", unique: true, using: :btree
  add_index "interesses", ["origem_id"], name: "index_interesses_on_origem_id", using: :btree

  create_table "n_habilitacoes", force: true do |t|
    t.string   "designacao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "noticia", force: true do |t|
    t.string   "titulo"
    t.date     "data"
    t.text     "sumario"
    t.text     "texto"
    t.boolean  "destaque"
    t.boolean  "activo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
  end

  create_table "oferta", force: true do |t|
    t.string   "titulo"
    t.text     "descricao"
    t.date     "dataI"
    t.date     "dataF"
    t.boolean  "activo"
    t.integer  "atividade_prof_id"
    t.integer  "entidade_id"
    t.integer  "tipo_contrato_id"
    t.integer  "salario_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "foto_file_name"
    t.string   "foto_content_type"
    t.integer  "foto_file_size"
    t.datetime "foto_updated_at"
  end

  add_index "oferta", ["atividade_prof_id", "created_at"], name: "index_oferta_on_atividade_prof_id_and_created_at", using: :btree
  add_index "oferta", ["entidade_id", "created_at"], name: "index_oferta_on_entidade_id_and_created_at", using: :btree
  add_index "oferta", ["salario_id", "created_at"], name: "index_oferta_on_salario_id_and_created_at", using: :btree
  add_index "oferta", ["tipo_contrato_id", "created_at"], name: "index_oferta_on_tipo_contrato_id_and_created_at", using: :btree

  create_table "salarios", force: true do |t|
    t.string   "designacao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "situacao_profissionals", force: true do |t|
    t.string   "designacao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_contratos", force: true do |t|
    t.string   "designacao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_users", force: true do |t|
    t.string   "designacao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "nome"
    t.string   "email"
    t.boolean  "activo"
    t.integer  "tipo_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.string   "remember_token"
    t.boolean  "subscription_newsletter"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["remember_token"], name: "index_users_on_remember_token", using: :btree
  add_index "users", ["tipo_user_id", "created_at"], name: "index_users_on_tipo_user_id_and_created_at", using: :btree

end
