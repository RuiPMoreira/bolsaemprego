# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
tipouser = TipoUser.create([{designacao: "admin"}, {designacao: "entidade"}, {designacao: "candidato"}])
atv = AtividadeProfissional.create([{designacao: "Informatica"}, {designacao: "Design"}, {designacao: "Recursos Humanos"}])
atv = Salario.create([{designacao: "600-1000"}, {designacao: "1000-1200"}, {designacao: "1200-1400"}])
atv = TipoContrato.create([{designacao: "full-time"}, {designacao: "part-time"}])
atv = Concelho.create([{designacao: "Leça"}, {designacao: "Lousada"}])
atv = AreaProfissional.create([{designacao: "Programador Web"}, {designacao: "Web Designer"}, {designacao: "Suporte"}])
atv = NHabilitacoes.create([{designacao: "Primario"}, {designacao: "Secundario"}, {designacao: "Superior"}])
atv = SituacaoProfissional.create([{designacao: "Empregado"}, {designacao: "Desempregado"}, {designacao: "Outro"}])
user = User.create([{nome: "Admin", email: "admin@wiremaze.pt", password: "123456", password_confirmation: "123456", activo: "true", tipo_user_id: "1"}])
#noticia = Noticium.create([{titulo: "ewewfefw", data: "2017-07-03", sumario: "sddssdfsdfsdffd", texto: "sdfsdfdfssdfsdffsdfsd", imagem: nil, destaque: true, activo: true, imagem_file_name: "images.png", imagem_content_type: "image/png", imagem_file_size: 1135, imagem_updated_at: "2017-03-01"}])
