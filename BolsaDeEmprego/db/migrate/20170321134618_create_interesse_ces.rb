class CreateInteresseCes < ActiveRecord::Migration
  def change
    create_table :interesse_ces do |t|
      t.integer :candidato_id
      t.integer :entidade_id

      t.timestamps
    end
    add_index :interesse_ces, [:candidato_id, :created_at]
    add_index :interesse_ces, [:entidade_id, :created_at]
    add_index :interesse_ces, [:candidato_id, :entidade_id], unique: true
  end
end
