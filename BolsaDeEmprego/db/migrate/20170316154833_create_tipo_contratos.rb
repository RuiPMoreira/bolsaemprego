class CreateTipoContratos < ActiveRecord::Migration
  def change
    create_table :tipo_contratos do |t|
      t.string :designacao

      t.timestamps
    end
  end
end
