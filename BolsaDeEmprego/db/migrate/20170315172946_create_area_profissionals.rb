class CreateAreaProfissionals < ActiveRecord::Migration
  def change
    create_table :area_profissionals do |t|
      t.string :designacao

      t.timestamps
    end
  end
end
