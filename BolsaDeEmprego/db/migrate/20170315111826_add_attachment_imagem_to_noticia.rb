include Paperclip::Schema
class AddAttachmentImagemToNoticia < ActiveRecord::Migration
  def self.up
    add_column :noticia, :imagem_file_name, :string
    add_column :noticia, :imagem_content_type, :string
    add_column :noticia, :imagem_file_size, :integer
    add_column :noticia, :imagem_updated_at, :datetime
  end

  def self.down
    remove_column :noticia, :imagem_file_name
    remove_column :noticia, :imagem_content_type
    remove_column :noticia, :imagem_file_size
    remove_column :noticia, :imagem_updated_at
  end
end
