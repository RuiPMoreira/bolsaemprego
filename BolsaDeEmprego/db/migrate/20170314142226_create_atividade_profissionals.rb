class CreateAtividadeProfissionals < ActiveRecord::Migration
  def change
    create_table :atividade_profissionals do |t|
      t.string :designacao

      t.timestamps
    end
  end
end
