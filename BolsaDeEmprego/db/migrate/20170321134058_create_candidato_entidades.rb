class CreateCandidatoEntidades < ActiveRecord::Migration
  def change
    create_table :candidato_entidades do |t|
      t.integer :candidato_id
      t.integer :entidade_id

      t.timestamps
    end
    add_index :candidato_entidades, [:candidato_id, :created_at]
    add_index :candidato_entidades, [:entidade_id, :created_at]
    add_index :candidato_entidades, [:candidato_id, :entidade_id], unique: true
  end
end
