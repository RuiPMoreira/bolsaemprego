class CreateCandidatos < ActiveRecord::Migration
  def change
    create_table :candidatos do |t|
      t.integer :user_id
      t.string :morada
      t.string :codigoPostal
      t.integer :contacto
      t.integer :contactoSec
      t.string :pagina
      t.date :data
      t.integer :bi
      t.integer :area_prof_id
      t.integer :habilitacao_id
      t.integer :situacao_prof_id
      t.integer :concelho_id
      t.text :apresentacao
      t.text :habilitacoes
      t.text :exp_profissional

      t.timestamps
    end
    add_index :candidatos, [:user_id, :created_at]
    add_index :candidatos, [:area_prof_id, :created_at]
    add_index :candidatos, [:habilitacao_id, :created_at]
    add_index :candidatos, [:situacao_prof_id, :created_at]
    add_index :candidatos, [:concelho_id, :created_at]
  end
end
