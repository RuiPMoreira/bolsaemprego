include Paperclip::Schema
class AddAttachmentFotoToEntidades < ActiveRecord::Migration
  def self.up
    add_column :entidades, :foto_file_name, :string
    add_column :entidades, :foto_content_type, :string
    add_column :entidades, :foto_file_size, :integer
    add_column :entidades, :foto_updated_at, :datetime
  end

  def self.down
    remove_column :entidades, :foto_file_name
    remove_column :entidades, :foto_content_type
    remove_column :entidades, :foto_file_size
    remove_column :entidades, :foto_updated_at
  end
end
