class AddAttachmentFotoToEntidades < ActiveRecord::Migration
  def self.up
    change_table :entidades do |t|
      t.attachment :foto
    end
  end

  def self.down
    remove_attachment :entidades, :foto
  end
end
