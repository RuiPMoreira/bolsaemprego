class CreateSituacaoProfissionals < ActiveRecord::Migration
  def change
    create_table :situacao_profissionals do |t|
      t.string :designacao

      t.timestamps
    end
  end
end
