class CreateOferta < ActiveRecord::Migration
  def change
    create_table :oferta do |t|
      t.string :titulo
      t.text :descricao
      t.date :dataI
      t.date :dataF
      t.boolean :activo
      t.integer :atividade_prof_id
      t.integer :entidade_id
      t.integer :tipo_contrato_id
      t.integer :salario_id


      t.timestamps
    end
    add_index :oferta, [:entidade_id, :created_at]
    add_index :oferta, [:atividade_prof_id, :created_at]
    add_index :oferta, [:tipo_contrato_id, :created_at]
    add_index :oferta, [:salario_id, :created_at]
  end
end
