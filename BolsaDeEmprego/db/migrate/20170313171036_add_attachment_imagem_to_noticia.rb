class AddAttachmentImagemToNoticia < ActiveRecord::Migration
  def self.up
    change_table :noticia do |t|
      t.attachment :imagem
    end
  end

  def self.down
    remove_attachment :noticia, :imagem
  end
end
