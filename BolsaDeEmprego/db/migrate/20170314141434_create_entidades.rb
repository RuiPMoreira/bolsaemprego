class CreateEntidades < ActiveRecord::Migration
  def change
    create_table :entidades do |t|
      t.integer :user_id
      t.string :morada
      t.string :codigoPostal
      t.integer :contacto
      t.integer :contactoSec
      t.string :pagina
      t.integer :nif
      t.integer :atividade_prof_id
      t.text :apresentacao
      t.integer :concelho_id

      t.timestamps
    end
    add_index :entidades, [:user_id, :created_at]
    add_index :entidades, [:atividade_prof_id, :created_at]
    add_index :entidades, [:concelho_id, :created_at]
  end
end
