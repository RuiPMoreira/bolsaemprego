class CreateNoticia < ActiveRecord::Migration
  def change
    create_table :noticia do |t|
      t.string :titulo
      t.date :data
      t.text :sumario
      t.text :texto
      t.boolean :destaque
      t.boolean :activo

      t.timestamps
    end
  end
end
