class CreateInteresses < ActiveRecord::Migration
  def change
    create_table :interesses do |t|
      t.integer :origem_id
      t.integer :destino_id

      t.timestamps
    end
    add_index :interesses, :origem_id
    add_index :interesses, :destino_id
    add_index :interesses, [:origem_id, :destino_id], unique: true
  end
end
