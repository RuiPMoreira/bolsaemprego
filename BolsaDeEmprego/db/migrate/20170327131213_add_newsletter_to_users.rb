class AddNewsletterToUsers < ActiveRecord::Migration
  def change
    add_column :users, :subscription_newsletter, :boolean
  end
end
