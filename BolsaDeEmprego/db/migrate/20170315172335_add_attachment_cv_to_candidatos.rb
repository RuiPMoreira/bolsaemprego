class AddAttachmentCvToCandidatos < ActiveRecord::Migration
  def self.up
    change_table :candidatos do |t|
      t.attachment :cv
    end
  end

  def self.down
    remove_attachment :candidatos, :cv
  end
end
