class CreateInteresseEcs < ActiveRecord::Migration
  def change
    create_table :interesse_ecs do |t|
      t.integer :entidade_id
      t.integer :candidato_id

      t.timestamps
    end
    add_index :interesse_ecs, [:entidade_id, :created_at]
    add_index :interesse_ecs, [:candidato_id, :created_at]
    add_index :interesse_ecs, [:entidade_id, :candidato_id], unique: true
  end
end
