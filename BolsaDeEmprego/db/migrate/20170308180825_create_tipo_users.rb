class CreateTipoUsers < ActiveRecord::Migration
  def change
    create_table :tipo_users do |t|
      t.string :designacao

      t.timestamps
    end
  end
end
