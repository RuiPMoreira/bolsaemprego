class AddAttachmentFotoToOferta < ActiveRecord::Migration
  def self.up
    change_table :oferta do |t|
      t.attachment :foto
    end
  end

  def self.down
    remove_attachment :oferta, :foto
  end
end
