class CreateConcelhos < ActiveRecord::Migration
  def change
    create_table :concelhos do |t|
      t.string :designacao

      t.timestamps
    end
  end
end
