class CreateNHabilitacoes < ActiveRecord::Migration
  def change
    create_table :n_habilitacoes do |t|
      t.string :designacao

      t.timestamps
    end
  end
end
