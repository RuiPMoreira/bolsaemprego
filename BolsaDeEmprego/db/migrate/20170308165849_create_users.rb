class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :nome
      t.string :email
      t.boolean :activo
      t.integer :tipo_user_id
      t.timestamps
    end
    add_index :users, [:tipo_user_id, :created_at]
    add_index :users, :email, unique: true
  end
end
