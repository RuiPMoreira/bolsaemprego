json.array!(@entidades) do |entidade|
  json.extract! entidade, :id
  json.url entidade_url(entidade, format: :json)
end
