json.array!(@noticia) do |noticium|
  json.extract! noticium, :id, :name
  json.url noticium_url(noticium, format: :json)
end
