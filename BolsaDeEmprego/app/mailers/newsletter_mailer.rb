class NewsletterMailer < ActionMailer::Base
  default from: "noreply@wiremaze.com"


  def weeknewsletter(user, entidades,candidatos,noticias,ofertas)
    @user = user
    @entidades = entidades
    @candidatos = candidatos
    @noticias = noticias
    @ofertas = ofertas

    Rails.logger.debug("---->My object2: #{candidatos.inspect}")
    mail to: user.email, subject: "Weekly Newsletter"
  end
end
