class Interesse < ActiveRecord::Base
  belongs_to :origem, class_name: "User"
  belongs_to :destino, class_name: "User"
end
