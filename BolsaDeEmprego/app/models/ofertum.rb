class Ofertum < ActiveRecord::Base
  belongs_to :entidade, inverse_of: :oferta
  validates :entidade_id, presence: true
  belongs_to :atividade_prof, inverse_of: :oferta, :class_name => 'AtividadeProfissional'
  validates :atividade_prof_id, presence: true
  belongs_to :tipo_contrato, inverse_of: :oferta, :class_name => 'TipoContrato'
  validates :tipo_contrato_id, presence: true
  belongs_to :salario, inverse_of: :oferta, :class_name => 'Salario'
  validates :salario_id, presence: true

  has_many :candidaturas, :class_name => 'Candidatura', :foreign_key => 'oferta_id'
  has_many :candidatos, through: :candidaturas



  has_attached_file :foto
  validates :foto, attachment_presence: true
  validates_attachment_content_type :foto, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  VALID_DATA_REGEX = /\A[0-9]{4}\-[0-9]{2}\-[0-9]{2}\z/i


  validates :titulo, length: { minimum:4,maximum: 50,  too_short: "Titulo demasiado curto", too_long: "Titulo demasiado comprido" }

  validates :dataI,  format: { with: VALID_DATA_REGEX, message: "Respeite o formato da data inicial (ex:dd-mm-yyyy)" }
  validates :dataF, format: { with: VALID_DATA_REGEX, message: "Respeite o formato da data final (ex:dd-mm-yyyy)" }
  validates :descricao, length: { minimum: 10,  too_short: "Conteudo demasiado curto (Minimo 10 caracteres)"}

  def existCandidaturaDe?(candidato)
    candidaturas.find_by(candidato_id: candidato.id)
  end

  def removerOferta!
    Candidatura.where(oferta_id: self.id).destroy_all
  end




  after_initialize do
    self.entidade ||= self.build_entidade()
    self.atividade_prof ||= self.build_atividade_prof()
    self.tipo_contrato ||= self.build_tipo_contrato()
    self.salario ||= self.build_salario()

  end


  def self.search(search,area,concelho)
    query = ""
    if !search || search.length==0
      query = "true"
    else
      query = "titulo LIKE '%#{search}%'"
    end
    query = query + " AND "

    if !area || area.length ==0
      query = query + "true"
    else
      query = query + "oferta.atividade_prof_id = '#{area}'"
    end

    query = query + " AND "

    if !concelho || concelho.length ==0
      query = query + "true"
    else
      query = query + "concelho_id = '#{concelho}'"
    end
    joins(:entidade).where(query)
  end
#  where('titulo LIKE ?', "%#{search}%")
#where('atividade_prof_id = ?', "#{area}")
#joins(:entidade).where('concelho_id = ?', "#{concelho}")

end
