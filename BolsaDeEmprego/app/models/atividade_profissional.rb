class AtividadeProfissional < ActiveRecord::Base
  has_many :entidade, inverse_of: :actividadeprof, :class_name => 'Entidade', :foreign_key => 'atividade_prof_id'
  accepts_nested_attributes_for :entidade
  has_many :oferta, inverse_of: :atividade_prof, :class_name => 'Oferta', :foreign_key => 'atividade_prof_id'
  accepts_nested_attributes_for :oferta
end
