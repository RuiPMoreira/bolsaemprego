class TipoContrato < ActiveRecord::Base
  has_many :oferta, inverse_of: :tipo_contrato, :class_name => 'Oferta', :foreign_key => 'tipo_contrato_id'
  accepts_nested_attributes_for :oferta
end
