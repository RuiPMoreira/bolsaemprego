class Noticium < ActiveRecord::Base
      has_attached_file :imagem
      validates :imagem, attachment_presence: true
      validates_attachment_content_type :imagem, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
      VALID_DATA_REGEX = /\A[0-9]{4}\-[0-9]{2}\-[0-9]{2}\z/i


      validates :titulo, length: { minimum:4,maximum: 50,  too_short: "Titulo demasiado curto", too_long: "Titulo demasiado comprido" }
      validates :data, format: { with: VALID_DATA_REGEX, message: "Respeite o formato da data (ex:dd-mm-yyyy)" }
      validates :texto, length: { minimum: 10,  too_short: "Conteudo demasiado curto (Minimo 10 caracteres)"}
      validates :sumario, length: { minimum: 10,  too_short: "Conteudo demasiado curto (Minimo 10 caracteres)"}


end
