class SituacaoProfissional < ActiveRecord::Base
  has_many :candidato, inverse_of: :situacao_prof, :class_name => 'Candidato', :foreign_key => 'situacao_prof_id'
  accepts_nested_attributes_for :candidato
end
