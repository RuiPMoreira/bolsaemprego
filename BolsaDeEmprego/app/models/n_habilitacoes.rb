class NHabilitacoes < ActiveRecord::Base
  has_many :candidato, inverse_of: :habilitacao, :class_name => 'Candidato', :foreign_key => 'habilitacao_id'
  accepts_nested_attributes_for :candidato
end
