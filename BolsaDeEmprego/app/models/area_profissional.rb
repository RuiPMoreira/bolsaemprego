class AreaProfissional < ActiveRecord::Base
  has_many :candidato, inverse_of: :area_prof, :class_name => 'Candidato', :foreign_key => 'area_prof_id'
  accepts_nested_attributes_for :candidato
end
