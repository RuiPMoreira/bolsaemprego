class Entidade < ActiveRecord::Base

  belongs_to :user, inverse_of: :entidade
  validates :user_id, presence: true
  belongs_to :actividadeprof, inverse_of: :entidade
  validates :atividade_prof_id, presence: true
  belongs_to :concelho, inverse_of: :entidade, :class_name => 'Concelho'
  validates :concelho_id, presence: true

  has_many :oferta, inverse_of: :entidade, :class_name => 'Ofertum', :foreign_key => 'entidade_id'
  accepts_nested_attributes_for :oferta

  has_attached_file :foto
  validates :foto, attachment_presence: true
  validates_attachment_content_type :foto, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

   VALID_CODPOSTAL_REGEX = /\A[0-9]{4}\-[0-9]{3}\z/i
   VALID_PAGINA_REGEX = /\A[a-z\d\-.]+\.[a-z]+\z/i


   validates :morada, presence: true, length: { maximum: 50,  too_long: "Morada demasiado comprida" }
   validates :apresentacao, length: { minimum: 10,  too_short: "Apresentação demasiado curta"}
   validates :codigoPostal, format: { with: VALID_CODPOSTAL_REGEX, message: "Respeite o formato do codigo postal (ex:4590-123)" }
   validates :pagina, format: { with: VALID_PAGINA_REGEX, message: "Respeite o formato do endereço web (ex:algo.dominio)" }

   validates :contacto, presence: true, :inclusion => { :in => 100000000..999999999, :message => "O contacto telefonico tem 9 digitos obrigatórios" }
   validates :contactoSec, :inclusion => { :in => 100000000..999999999, :message => "O contacto telefonico alternativo tem 9 digitos obrigatórios", :allow_nil => true }
   validates :nif, :inclusion => { :in => 100000000..999999999, :message => "O NIF tem 9 digitos obrigatórios" }


  after_initialize do
    self.user ||= self.build_user()
    self.concelho ||= self.build_concelho()
    self.oferta ||= self.build_oferta()
  end

  def self.search(search,area,concelho)
    query = ""
    if !search || search.length==0
      query = "true"
    else
      query = "(nome LIKE '%#{search}%' OR apresentacao LIKE '%#{search}%')"
    end
    query = query + " AND "

    if !area || area.length ==0
      query = query + "true"
    else
      query = query + "atividade_prof_id = '#{area}'"
    end

    query = query + " AND "

    if !concelho || concelho.length ==0
      query = query + "true"
    else
      query = query + "concelho_id = '#{concelho}'"
    end
    joins(:user).where(query)
  end
end
