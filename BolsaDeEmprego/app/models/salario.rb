class Salario < ActiveRecord::Base
  has_many :oferta, inverse_of: :salario, :class_name => 'Oferta', :foreign_key => 'salario_id'
  accepts_nested_attributes_for :oferta
end
