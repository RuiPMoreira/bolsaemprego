class Candidatura < ActiveRecord::Base
  belongs_to :candidato
  belongs_to :oferta, :class_name => 'Ofertum'
end
