class TipoUser < ActiveRecord::Base
  has_many :user, inverse_of: :tipo_user, :class_name => 'User', :foreign_key => 'tipo_user_id'
  accepts_nested_attributes_for :user


end
