class Candidato < ActiveRecord::Base
  belongs_to :user, inverse_of: :candidato
  validates :user_id, presence: true
  belongs_to :area_prof, inverse_of: :candidato, :class_name => 'AreaProfissional'
  validates :area_prof_id, presence: true
  accepts_nested_attributes_for :area_prof
  belongs_to :habilitacao, inverse_of: :candidato, :class_name => 'NHabilitacoes'
  validates :habilitacao_id, presence: true
  belongs_to :situacao_prof, inverse_of: :candidato, :class_name => 'SituacaoProfissional'
  validates :situacao_prof_id, presence: true
  belongs_to :concelho, inverse_of: :candidato, :class_name => 'Concelho'
  validates :concelho_id, presence: true

  has_many :candidaturas
  has_many :ofertas, :class_name => 'Ofertum', through: :candidaturas

  has_attached_file :foto
  validates :foto, attachment_presence: true
  validates_attachment_content_type :foto, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates :cv, attachment_presence: true
  has_attached_file :cv


  def existCandidaturaPara?(oferta)
    candidaturas.find_by(oferta_id: oferta.id)
  end

  def candidatar!(oferta)
    candidaturas.create!(oferta_id: oferta.id)
  end

  def retirarCandidatura!(oferta)
    candidaturas.find_by(oferta_id: oferta.id).destroy
  end

  VALID_CODPOSTAL_REGEX = /\A[0-9]{4}\-[0-9]{3}\z/i
  VALID_DATA_REGEX = /\A[0-9]{4}\-[0-9]{2}\-[0-9]{2}\z/i
  VALID_PAGINA_REGEX = /\A[a-z\d\-.]+\.[a-z]+\z/i


  validates :morada, presence: true, length: { maximum: 50,  too_long: "Morada demasiado comprida" }
  validates :data, format: { with: VALID_DATA_REGEX, message: "Respeite o formato da data (ex:dd-mm-yyyy)" }
  validates :apresentacao, length: { minimum: 10,  too_short: "Apresentação demasiado curta"}
  validates :habilitacoes, length: { minimum: 10,  too_short: "Habilitações deverá conter pelo menos 10 caracteres"}
  validates :exp_profissional, length: { minimum: 10,  too_short: "Experiencia Profissional deverá conter pelo menos 10 caracteres"}
  validates :codigoPostal, format: { with: VALID_CODPOSTAL_REGEX, message: "Respeite o formato do codigo postal (ex:4590-123)" }
  validates :pagina, format: { with: VALID_PAGINA_REGEX, message: "Respeite o formato do endereço web (ex:algo.dominio)" }

  validates :contacto, presence: true, :inclusion => { :in => 100000000..999999999, :message => "O contacto telefonico tem 9 digitos obrigatórios" }
  validates :contactoSec, :inclusion => { :in => 100000000..999999999, :message => "O contacto telefonico alternativo tem 9 digitos obrigatórios", :allow_nil => true }
  validates :bi, presence: true, :inclusion => { :in => 10000000..99999999, :message => "O BI tem 8 digitos obrigatórios" }

  after_initialize do
    self.user ||= self.build_user()

    #Rails.logger.debug("---->My object43: #{( AreaProfissional.find(self.area_prof_id) ).attributes.inspect}")
    self.area_prof ||= self.build_area_prof()
    self.situacao_prof ||= self.build_situacao_prof()
    self.habilitacao ||= self.build_habilitacao()
    self.concelho ||= self.build_concelho()

    #Rails.logger.debug("---->My object2: #{(self.area_prof ).inspect}")
  end

  def self.search(search,area,stituacao, concelho)
    query = ""
    if !search || search.length==0
      query = "true"
    else
      query = "(nome LIKE '%#{search}%' OR apresentacao LIKE '%#{search}%' OR habilitacoes LIKE '%#{search}%'  OR exp_profissional LIKE '%#{search}%')"
    end
    query = query + " AND "

    if !area || area.length ==0
      query = query + "true"
    else
      query = query + "area_prof_id = '#{area}'"
    end

    query = query + " AND "

    if !stituacao || stituacao.length ==0
      query = query + "true"
    else
      query = query + "situacao_prof_id = '#{area}'"
    end

    query = query + " AND "

    if !concelho || concelho.length ==0
      query = query + "true"
    else
      query = query + "concelho_id = '#{concelho}'"
    end
    joins(:user).where(query)
  end
end
