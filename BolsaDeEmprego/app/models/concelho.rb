class Concelho < ActiveRecord::Base
  has_many :candidato, inverse_of: :concelho, :class_name => 'Candidato', :foreign_key => 'concelho_id'
  accepts_nested_attributes_for :candidato
  has_many :entidade, inverse_of: :concelho, :class_name => 'Entidade', :foreign_key => 'concelho_id'
  accepts_nested_attributes_for :entidade
end
