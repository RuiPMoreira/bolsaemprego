class User < ActiveRecord::Base
  attr_accessor :reset_token
    before_save { self.email = email.downcase }


    belongs_to :tipo_user, inverse_of: :user, :class_name => 'TipoUser'
    validates :tipo_user_id, presence: true

    has_one :entidade, inverse_of: :user, :class_name => 'Entidade', :foreign_key => 'user_id', dependent: :destroy
    accepts_nested_attributes_for :entidade
    has_one :candidato, inverse_of: :user, :class_name => 'Candidato', :foreign_key => 'user_id', dependent: :destroy
    accepts_nested_attributes_for :candidato


    has_many :interesses, :class_name => 'Interesse', foreign_key: "origem_id", dependent: :destroy
    has_many :interessadoEm, through: :interesses, source: :destino
    has_many :reverse_interesses, foreign_key: "destino_id",
                                     class_name:  "Interesse",
                                     dependent:   :destroy
    has_many :interessadosEmMim, through: :reverse_interesses, source: :origem



    validates :nome, presence: true, length: { maximum: 50,  too_long: "Nome demasiado comprido" }
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, format: { with: VALID_EMAIL_REGEX, message: "Respeite o formato do email (exemplo@fornecedor.dominio)"},
                       uniqueness: { case_sensitive: false }
    has_secure_password
    validates :password, length: { minimum: 6,  too_short: "Password demasiado curta (Insegura)"},  :if => :password



    def following?(other_user)
      interesses.find_by(destino_id: other_user.id)
    end

    def follow!(other_user)
      interesses.create!(destino_id: other_user.id)
    end

    def unfollow!(other_user)
      interesses.find_by(destino_id: other_user.id).destroy
    end


    def User.new_remember_token
      SecureRandom.urlsafe_base64
    end

    def User.digest(token)
      Digest::SHA1.hexdigest(token.to_s)
    end

    # Sets the password reset attributes.
    def create_reset_digest
    self.reset_token = User.new_remember_token
      update_attribute(:reset_digest,  User.digest(reset_token))
      update_attribute(:reset_sent_at, Time.zone.now)
    end




    after_initialize do
      self.tipo_user ||= self.build_tipo_user()
    end

    private

  end
