// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require_tree
//= require tinymce-jquery
//= require jquery.ui.datepicker

function updateSearch()
{
  var i = 0, pos;
  var ths = document.getElementsByTagName("th");
  for(i = 0; i<ths.length-1; i++)
  {
    var campoTh =ths[i].childNodes[0].nodeValue.toLowerCase();
    var campoTipoPesquisa = document.getElementById("searchType").value.toLowerCase();
    if(campoTh==campoTipoPesquisa)
    {
        pos = i;
    }
  }
  var trs = document.getElementsByTagName("tr");

  for(i=1;i<trs.length;i++)
  {
    trs[i].removeAttribute("class");

    var campoLinha = trs[i].childNodes[pos*2+1].childNodes[0].nodeValue.toLowerCase();
    var campoPesquisa = document.getElementById("search").value;
    if(campoLinha.indexOf(campoPesquisa)==-1)
    {
      trs[i].className += "hide";
    }
  }

}

function updateSearchSimple()
{
  var i = 0;

  var trs = document.getElementsByTagName("tr");

  for(i=1;i<trs.length;i++)
  {
    trs[i].removeAttribute("class");

    var campoLinha = trs[i].childNodes[1].childNodes[0].nodeValue.toLowerCase();
    var campoPesquisa = document.getElementById("search").value;
    if(campoLinha.indexOf(campoPesquisa)==-1)
    {
      trs[i].className += "hide";
    }
  }

}

  $("#newsletter").on("click", "a", function() {
  $("#flash").html('<%= j render partial: "shared/notice_banner" %>');
});

$(function() {

  $("#oferta_search input").keyup(function() {
    $.get($("#oferta_search").attr("action"), $("#oferta_search").serialize(), null, "script");
    return false;
  });
});
