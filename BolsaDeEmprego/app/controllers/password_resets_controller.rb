class PasswordResetsController < ApplicationController
  before_action :get_user,   only: [:edit, :update]

  def new

  end
  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      UserMailer.password_reset(@user).deliver

      flash[:info] = "Foi enviado um email com intruções para recuperação da password"
      redirect_to root_url
    else
      flash.now[:danger] = "Endereço de email não foi encontrado"
      render 'new'
    end
  end

  def edit
  end


    def update
      if params[:user][:password].empty?                  # Case (3)
        @user.errors.add(:password, "Campo não pode estar vazio")
        render 'edit'
      elsif @user.update_attributes(user_params)
        flash[:success] = "Nova password atribuida"
        redirect_to signin_path
      else
        render 'edit'                                     # Case (2)
      end
    end
  private

   def get_user
     @user = User.find_by(email: params[:email])
   end

   def user_params
     params.require(:user).permit(:password, :password_confirmation)
   end
end
