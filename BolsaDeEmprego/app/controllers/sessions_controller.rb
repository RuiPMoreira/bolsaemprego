class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if(!signed_in?)
        sign_in user
      else
        sign_out
        sign_in user
      end
      
      redirect_to profileHelper
    else
      flash.now[:error] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_url
  end

  private

  def profileHelper
    if current_user.tipo_user.designacao == 'entidade'
      "#{entidade_profile_path(current_user.entidade.id)}"
    else
      if current_user.tipo_user.designacao == 'candidato'
        "#{candidato_profile_path(current_user.candidato.id)}"
      else
        if current_user.tipo_user.designacao == 'admin'
          "#{home_backoffice_path}"
        end
      end
    end
  end
end
