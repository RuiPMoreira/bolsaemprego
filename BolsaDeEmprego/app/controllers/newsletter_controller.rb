class NewsletterController < ApplicationController
  def new
    @users=User.where("subscription_newsletter=true")
    @entidades = Entidade.where("created_at > ?", Date.today - 1.week)
    @candidatos =Candidato.where("created_at > ?", Date.today - 1.week)
    @noticias =Noticium.where("activo=true AND created_at > ?", Date.today - 1.week)
    @ofertas =Ofertum.where("activo=true AND created_at > ?", Date.today - 1.week)
    Rails.logger.debug("---->My object: #{@candidatos.inspect}")
    @users.each do |user|
      if user
        NewsletterMailer.weeknewsletter(user,@entidades,@candidatos,@noticias,@ofertas).deliver
      end
    end
    respond_to do |format|
      format.js { flash.now[:notice] = "Here is my flash notice" }
    end
  end
end
