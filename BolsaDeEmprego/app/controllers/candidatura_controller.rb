class CandidaturaController < ApplicationController
  before_action :is_logged

  def create
    @oferta = Ofertum.find(params[:id])

    current_user.candidato.candidatar!(@oferta)

    respond_to do |format|
      format.html { redirect_to :back}
      format.json { head :no_content }
    end
  end

  def destroy
    @oferta = Ofertum.find(params[:id])

    current_user.candidato.retirarCandidatura!(@oferta)
    respond_to do |format|
      format.html { redirect_to :back}
      format.json { head :no_content }
    end
  end

  private
    def is_logged
      if(!signed_in?)
        render :file => 'public/permition.html', :layout => false
      else
        user = User.find(current_user.id)
        tipouser = TipoUser.find(user.tipo_user_id)

        if(tipouser.designacao != 'candidato')
          render :file => 'public/permition.html', :layout => false
        end
      end
    end

end
