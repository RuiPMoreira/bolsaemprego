class NoticiaController < ApplicationController
  before_action :is_logged, except: [:indexFrontOffice,:showFrontOffice]
#before_action :set_noticium, only: [:show, :edit, :update, :destroy]


  def index
    @noticia = Noticium.all
  end

  def show
    @noticium = Noticium.find(params[:id])
  end

  def indexFrontOffice
    @noticium = Noticium.all
    @noticias = @noticium.where("activo=true").paginate(page: params[:page], :per_page => 10, :order => "destaque DESC, data DESC")
  end

  def showFrontOffice
    @noticium = Noticium.find(params[:id])
  end

  def new
    @noticium = Noticium.new
  end

  def edit
    @noticium = Noticium.find(params[:id])
  end

  def create
    @noticium = Noticium.new(noticium_params)
    Rails.logger.debug("---->My object: #{@noticium.inspect}")

      if @noticium.save
        flash[:success] = "Noticia registada"
        redirect_to @noticium
      else
        render 'new'
      end

  end

  def update
    @noticium = Noticium.find(params[:id])

    if @noticium.update_attributes(noticium_params)
      flash[:success] = "Noticia actualizada"
      redirect_to @noticium
    else
      render 'edit'
    end
  end


  def destroy
    @noticium.destroy
    respond_to do |format|
      format.html { redirect_to noticia_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_noticium
      @noticium = Noticium.find(params[:id])
    end

    def is_logged
      if(!signed_in?)
        render :file => 'public/permition.html', :layout => false
      else
        user = User.find(current_user.id)
        tipouser = TipoUser.find(user.tipo_user_id)

        if(tipouser.designacao != 'admin')
          render :file => 'public/permition.html', :layout => false
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def noticium_params
      params.require(:noticium).permit(:titulo, :sumario, :data, :texto, :imagem, :destaque, :activo)
    end
end
