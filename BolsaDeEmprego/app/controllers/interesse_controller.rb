class InteresseController < ApplicationController
  before_action :is_logged
  def create
    @user = User.find(params[:id])

    current_user.follow!(@user)

    respond_to do |format|
      format.html { redirect_to :back}
      format.json { head :no_content }
    end
  end

  def destroy
    @user = User.find(params[:id])
    current_user.unfollow!(@user)
    respond_to do |format|
      format.html { redirect_to :back}
      format.json { head :no_content }
    end
  end

  private
    def is_logged
      if(!signed_in?)
        render :file => 'public/permition.html', :layout => false
      else
        user = User.find(current_user.id)
        tipouser = TipoUser.find(user.tipo_user_id)

        if(tipouser.designacao != 'entidade' && tipouser.designacao != 'candidato')
          render :file => 'public/permition.html', :layout => false
        end
      end
    end
end
