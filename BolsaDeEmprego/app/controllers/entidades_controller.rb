class EntidadesController < ApplicationController
  before_action :is_logged, except: [:index,:show,:new, :create]

  # GET /entidades
  # GET /entidades.json
  def index
    #Rails.logger.debug("---->My object2: #{(params).inspect}")
    if !params[:search] && !params[:area] && !params[:concelho]
      @entidades = Entidade.where("activo=true").search("","","").paginate(:per_page => 8, :page => params[:page])
    else
      @entidades = Entidade.where("activo=true").search(params[:search],params[:area][:id],params[:concelho][:id]).paginate(:per_page => 8, :page => params[:page])
    end
  end



  # GET /entidades/1
  # GET /entidades/1.json
  def show
    @entidade = Entidade.find(params[:id])
    @atividade = AtividadeProfissional.find(@entidade.atividade_prof_id)
    @user=User.find(@entidade.user_id)
    @ofertas= Ofertum.where("activo=true AND entidade_id=?", @entidade.id).find(:all, :order => "id desc", :limit => 4).reverse;

  end


  # GET /entidades/1
  # GET /entidades/1.json
  def profile
    @entidade = Entidade.find(params[:id])
  end

  # GET /entidades/new
  def new
    @entidade = Entidade.new
    @entidade.build_user
      @user = User.new
      @atividades = AtividadeProfissional.all
  end

  # GET /entidades/1/edit
  def edit
    @entidade = Entidade.find(params[:id])
        #Rails.logger.debug("---->My object2: #{(@entidade).inspect}")
    @user=User.find(@entidade.user_id)

    @entidade.user=User.find(@entidade.user_id)
    @entidade.build_user
      #  Rails.logger.debug("---->My object2: #{( @entidade).inspect}")

    @atividades = AtividadeProfissional.all
  end

  def create
    @tipo_user_id = TipoUser.find_by(designacao: "entidade").id
    @user = User.new(user_params.merge(tipo_user_id: @tipo_user_id))
    @user.activo = true
    @entidade = Entidade.new(entidade_params)
    @entidade.valid?

  if @user.save
      @entidade = Entidade.new(entidade_params.merge(user_id: @user.id))

        if @entidade.save
          flash[:success] = "Registado com sucesso"
          redirect_to signin_path
        else
          User.delete(@user.id)
          render "new"
        end
    else
      render "new"
    end
  end

  # PATCH/PUT /entidades/1
  # PATCH/PUT /entidades/1.json
  def update
    @entidade = Entidade.find(params[:id])
    @user = User.find(@entidade.user_id)

    if @user.update_attributes(user_params)
      if @entidade.update_attributes(entidade_params)
        flash[:success] = "Perfil Actualizado"
        redirect_to @entidade
      else
        render 'edit'
      end
    else
      render 'edit'
    end

  end

  # DELETE /entidades/1
  # DELETE /entidades/1.json
  def destroy
    @entidade.destroy
    respond_to do |format|
      format.html { redirect_to entidades_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entidade
      @entidade = Entidade.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.

    def user_params
        params.require(:entidade).require(:user).permit(:nome, :email, :password, :password_confirmation, :subscription_newsletter )
    end
    def entidade_params
        params.require(:entidade).permit(:user_id, :morada, :foto, :codigoPostal, :concelho_id, :contacto, :contactoSec, :pagina, :nif, :apresentacao, :atividade_prof_id)
    end

    def is_logged
      if(!signed_in?)
        render :file => 'public/permition.html', :layout => false
      else
        user = User.find(current_user.id)
        tipouser = TipoUser.find(user.tipo_user_id)

        if(tipouser.designacao != 'entidade' && tipouser.designacao != 'admin')
          render :file => 'public/permition.html', :layout => false
        end
      end
    end
end
