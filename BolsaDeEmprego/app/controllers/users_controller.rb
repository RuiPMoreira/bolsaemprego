class UsersController < ApplicationController
  before_action :is_logged, except: [:regist,:editpasswordFO,:updatePassword]

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    @tipo = TipoUser.find(@user.tipo_user_id)
    @activo = ""
    if @user.activo == true
      @activo = "Sim"
    else
      @activo = "Não"
    end
  end

  def new
    @user = User.new
  end

  def regist

  end


  def edit
      @user = User.find(params[:id])
      @tipo = TipoUser.find(@user.tipo_user_id)
  end

  def editpasswordFO
    if(!signed_in?)
      render :file => 'public/permition.html', :layout => false
    else
      if(current_user.id != params[:id].to_i)
        render :file => 'public/permition.html', :layout => false
      end
    end
    @user = User.find(params[:id])
    @tipo = TipoUser.find(@user.tipo_user_id)

  end

  def editpassword
      @user = User.find(params[:id])
      @tipo = TipoUser.find(@user.tipo_user_id)
  end

  def create
    @tipo_user_id = TipoUser.find_by(designacao: "admin").id
    @user = User.new(user_params.merge(password: '123456', password_confirmation: '123456',tipo_user_id: @tipo_user_id))                                             #################Atualizar
    #@user.activo = 1;
    if @user.save
      flash[:success] = "Bem vindo"
      redirect_to @user
    else

      render 'new'
    end
  end

  def update

    @user = User.find(params[:id])
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password_confirmation].blank?
    if @user.update_attributes(user_params)
      flash[:success] = "Perfil Actualizado"
      redirect_to @user
    else
          Rails.logger.debug("---->My object2: #{( @user.errors.full_messages).inspect}")
      render 'edit'
    end
  end

  def updatePassword
    if(!signed_in?)
      render :file => 'public/permition.html', :layout => false
    else
      if(current_user.id != params[:id].to_i)
        render :file => 'public/permition.html', :layout => false
      end
    end
    @user = User.find(params[:id])

    if @user && @user.authenticate(params[:user][:passwordActual])
      if @user.update_attributes(user_params)
        flash[:success] = "Perfil Actualizado"
        if(current_user.tipo_user.designacao=="entidade")
          redirect_to entidade_profile_path(current_user.entidade.id)
        else
          if(current_user.tipo_user.designacao=="candidato")
              redirect_to candidato_profile_path(current_user.candidato.id)
          end
        end
      else
        render 'editpasswordFO'
      end
    else
      @user.errors.add(:passwordActual, "Introduza a password actual")
      render 'editpasswordFO'
    end
  end


  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Utilizador Eliminado"
    redirect_to :controller => 'users', :action => 'index'

  end

  private

    def user_params
      params.require(:user).permit(:nome, :email, :password,
                                   :password_confirmation, :activo, :subscription_newsletter )
    end

    def is_logged
      if(!signed_in?)
        render :file => 'public/permition.html', :layout => false
      else
        user = User.find(current_user.id)
        tipouser = TipoUser.find(user.tipo_user_id)

        if(tipouser.designacao != 'admin')
          render :file => 'public/permition.html', :layout => false
        end
      end
    end
end
