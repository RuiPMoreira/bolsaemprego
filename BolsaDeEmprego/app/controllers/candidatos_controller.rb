class CandidatosController < ApplicationController
  before_action :is_logged, except: [:index,:show,:new, :create]

  # GET /candidatos
  # GET /candidatos.json
  def index
    if !params[:search] && !params[:area] && !params[:situacao] && !params[:concelho]
      @candidatos = Candidato.where("activo=true").search("","","","").paginate(:per_page => 8, :page => params[:page])
    else
      @candidatos = Candidato.where("activo=true").search(params[:search],params[:area][:id], params[:situacao][:id],params[:concelho][:id]).paginate(:per_page => 8, :page => params[:page])
    end
  end

  # GET /candidatos/1
  # GET /candidatos/1.json
  def profile
    @candidato = Candidato.find(params[:id])
    @user=@candidato.user
    @other_user = Entidade.find(1).user
  end


  # GET /candidatos/1
  # GET /candidatos/1.json
  def show
    @candidato = Candidato.find(params[:id])
  end

  # GET /candidatos/new
  def new
    @candidato = Candidato.new
    @candidato.build_user
    @candidato.build_area_prof
    @user = User.new
  end

  # GET /candidatos/1/edit
  def edit
    @candidato = Candidato.find(params[:id])
    @user=User.find(@candidato.user_id)

    @candidato.user=User.find(@candidato.user_id)
    @candidato.build_user
    #@candidato.build_area_prof

  end

  # POST /candidatos
  # POST /candidatos.json
  def create
    @tipo_user_id = TipoUser.find_by(designacao: "candidato").id
    @user = User.new(user_params.merge(tipo_user_id: @tipo_user_id,))
    @user.activo = true
    @user.subscription_newsletter=true
    @candidato = Candidato.new(candidato_params)
    @candidato.valid?
    Rails.logger.debug("---->My object2: #{( @candidato.errors.full_messages).inspect}")


  if @user.save
      @candidato = Candidato.new(candidato_params.merge(user_id: @user.id))

        if @candidato.save

          flash[:success] = "Registado com sucesso"
          redirect_to signin_path
        else
          User.delete(@user.id)
          render "new"
        end
    else
      render "new"
    end
  end

  # PATCH/PUT /candidatos/1
  # PATCH/PUT /candidatos/1.json
  def update
    @candidato = Candidato.find(params[:id])
    @user = User.find(@candidato.user_id)

    if @user.update_attributes(user_params)
      if @candidato.update_attributes(candidato_params)
        flash[:success] = "Perfil Actualizado"
        redirect_to @candidato
      else
        render 'edit'
      end
    else
      render 'edit'
    end
  end

  # DELETE /candidatos/1
  # DELETE /candidatos/1.json
  def destroy

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_candidato
      @candidato = Candidato.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
        params.require(:candidato).require(:user).permit(:nome, :email, :password, :password_confirmation, :subscription_newsletter )
    end
    def candidato_params
        params.require(:candidato).permit(:user_id, :morada, :foto, :codigoPostal, :concelho_id, :contacto, :contactoSec, :pagina, :bi, :apresentacao, :area_prof_id, :cv,:habilitacao_id, :habilitacoes, :situacao_prof_id, :exp_profissional, :data)
    end

    def is_logged
      if(!signed_in?)
        render :file => 'public/permition.html', :layout => false
      else
        user = User.find(current_user.id)
        tipouser = TipoUser.find(user.tipo_user_id)

        if(tipouser.designacao != 'candidato' && tipouser.designacao != 'admin')
          render :file => 'public/permition.html', :layout => false
        end
      end
    end
end
