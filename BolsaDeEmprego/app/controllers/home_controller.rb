class HomeController < ApplicationController
before_action :is_logged, except: [:index]
#before_action :set_noticium, only: [:show, :edit, :update, :destroy]


  def index
    @noticias= Noticium.where("activo=true").find(:all, :order => "destaque DESC, created_at DESC", :limit => 3).reverse;
    @ofertas= Ofertum.where("activo=true").find(:all, :order => "id desc", :limit => 3).reverse;
    @empresas= Entidade.joins(:user).where("activo=true").find(:all, :order => "id desc", :limit => 3).reverse;
    @candidatos= Candidato.joins(:user).where("activo=true").find(:all, :order => "id desc", :limit => 3).reverse;

    #Noticium.find(:all, :order => "id desc", :limit => 3).reverse
  end

  def indexBackOffice
    render :inline => "<div id='flash'>
      <% if notice.present? %>
        <%= render partial: 'shared/notice_banner' %>
      <% end %>
    </div>", :layout => true
  end

  private
    def is_logged
      if(!signed_in?)
        render :file => 'public/permition.html', :layout => false
      else
        user = User.find(current_user.id)
        tipouser = TipoUser.find(user.tipo_user_id)

        if(tipouser.designacao != 'admin')
          render :file => 'public/permition.html', :layout => false
        end
      end
    end
end
