class OfertaController < ApplicationController
    before_action :is_logged, only: [:new,:create,:destroy], except: [:index,:show]
    before_action :is_logged_as_entidade, except: [:index,:show]



  def index
    if !params[:search] && !params[:area] && !params[:concelho]
      @oferta = Ofertum.where("activo=true").search("","","").paginate(:per_page => 8, :page => params[:page])
    else
      @oferta = Ofertum.where("activo=true").search(params[:search],params[:area][:id],params[:concelho][:id]).paginate(:per_page => 8, :page => params[:page])
    end

  end


  def show
    @oferta = Ofertum.find(params[:id])
    @ofertadaarea = Ofertum.where("atividade_prof_id=? AND id != ?", @oferta.atividade_prof_id,@oferta.id)
  end


  def new
    @oferta = Ofertum.new
    @oferta.build_entidade
    @oferta.entidade=Entidade.new(Entidade.find(1).attributes)
    @entidade = current_user.entidade
  end


  def edit
    @oferta = Ofertum.find(params[:id])
    @oferta.build_entidade
    @oferta.entidade=Entidade.new(Entidade.find(1).attributes)
    @entidade = Entidade.find(@oferta.entidade_id)
  end

  def create
    @oferta = Ofertum.new(ofertum_params)
    @entidade = current_user.entidade

      if @oferta.save
          flash[:success] = "Oferta Registada"
          redirect_to @oferta
      else
        render 'new'
      end
  end


  def update
    @oferta = Ofertum.find(params[:id])
    @entidade = @oferta.entidade
    if @oferta.update_attributes(ofertum_params)
      flash[:success] = "Oferta Actualizada"
      redirect_to @oferta
    else
      render 'edit'
    end
  end

  def activar
    @ofertum = Ofertum.find(params[:id])
    @ofertum.update_attributes(activo: 'true')
    respond_to do |format|
      format.html { redirect_to :back}
      format.json { head :no_content }
    end
  end

  def desactivar
    @ofertum = Ofertum.find(params[:id])
    @ofertum.update_attributes(activo: 'false')
    respond_to do |format|
      format.html { redirect_to :back}
      format.json { head :no_content }
    end
  end

  def destroy
    @ofertum.destroy
    respond_to do |format|
      format.html { redirect_to oferta_url }
      format.json { head :no_content }
    end
  end

  private
    def set_ofertum
      @ofertum = Ofertum.find(params[:id])
    end

    def ofertum_params
      params.require(:ofertum).permit(:titulo, :dataI, :dataF, :descricao, :foto, :entidade_id, :atividade_prof_id, :tipo_contrato_id,:salario_id, :activo)
    end

    def is_logged
      if(!signed_in?)
        render :file => 'public/permition.html', :layout => false
      else
        user = User.find(current_user.id)
        tipouser = TipoUser.find(user.tipo_user_id)

        if(tipouser.designacao != 'entidade' && tipouser.designacao != 'admin')
          render :file => 'public/permition.html', :layout => false
        end
      end
    end
    def is_logged_as_entidade
      if(!signed_in?)
        render :file => 'public/permition.html', :layout => false
      else
        user = User.find(current_user.id)
        tipouser = TipoUser.find(user.tipo_user_id)

        if(tipouser.designacao != 'entidade')
          render :file => 'public/permition.html', :layout => false
        end
      end
    end
end
