module ApplicationHelper
  def extlink(link)

   if link.include?("http://")
    puts link
   else
    link.insert(0, "http://")
    link
   end
 end
 def containsBackoffice
   index = request.original_url.index("backoffice")
   if index && index > -1
     request.original_url[index-1]=='/'&& true#request.original_url[index+10]=='/'
   else
     false
   end
 end
 def profileHelper
   if current_user.tipo_user.designacao == 'entidade'
     "#{entidade_profile_path(current_user.entidade.id)}"
   else
     if current_user.tipo_user.designacao == 'candidato'
       "#{candidato_profile_path(current_user.candidato.id)}"
     else
       if current_user.tipo_user.designacao == 'admin'
         "#{home_backoffice_path}"
       end
     end
   end
 end

 def currentUserIsAdmin?
   if signed_in? && current_user.tipo_user.designacao == 'admin'
     true
   else
     false
   end
 end
 def currentUserIsEntidade?
   if signed_in? && current_user.tipo_user.designacao == 'entidade'
     true
   else
     false
   end
 end
 def currentUserIsCandidato?
   if signed_in? && current_user.tipo_user.designacao == 'candidato'
     true
   else
     false
   end
 end
 def show_errors(object, field_name)
  if object.errors.any?
    if !object.errors.messages[field_name].blank?
      object.errors.messages[field_name].join(", ")
    end
  end
end 
end
