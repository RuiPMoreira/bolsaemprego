require 'test_helper'

class NewsletterMailerTest < ActionMailer::TestCase
  test "weeknewsletter" do
    mail = NewsletterMailer.weeknewsletter
    assert_equal "Weeknewsletter", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
